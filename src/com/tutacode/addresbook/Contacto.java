package com.tutacode.addresbook;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Contacto implements Serializable{
	private static final long serialVersionUID = -7060210544600464481L;
	public String nombre;
	public String foto;
	public List<String> contact_ids;
	public List<String> telefonos;
	public List<String> emails;

	public Contacto(){
		nombre = "";
		foto = "";
		contact_ids = new ArrayList<String>();
		telefonos = new ArrayList<String>();
		emails = new ArrayList<String>();
	}
}