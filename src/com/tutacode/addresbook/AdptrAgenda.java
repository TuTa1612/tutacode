package com.tutacode.addresbook;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tutacode.R;

public class AdptrAgenda extends BaseAdapter {
	Context mContext;
	List<Contacto> contactos;
	
	public AdptrAgenda(Context ctx){
		mContext = ctx;
		contactos = new ArrayList<Contacto>();
	}
	
	public void setSource(List<Contacto> items){
		contactos = items;
		notifyDataSetChanged();
	}
	
	public void insertNewItem(Contacto unItem){
		contactos.add(unItem);
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return contactos!=null ?contactos.size() :0;
	}

	@Override
	public Contacto getItem(int position) {
		return getCount()>0 && getCount()>position ?contactos.get(position) :null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView==null){
			convertView = LayoutInflater.from(mContext).inflate(R.layout.celda_contacto, parent, false);
			holder = new ViewHolder();
			holder.imgAvatar = (ImageView) convertView.findViewById(R.id.imgContactoAvatar);
			holder.tvNombre = (TextView) convertView.findViewById(R.id.tvContactoNombre);
			convertView.setTag(holder);
		} else{
			holder = (ViewHolder) convertView.getTag();
		}
	
		final Contacto unContacto = getItem(position);
		holder.tvNombre.setText(unContacto.nombre);
		if(unContacto.foto!=null && unContacto.foto.trim().length()>0){
			Picasso.with(mContext).load(unContacto.foto).placeholder(R.drawable.contact).error(R.drawable.contact).into(holder.imgAvatar);
			//new ImageLoader(mContext, holder.imgAvatar).execute(unContacto.foto);
		}else{
			holder.imgAvatar.setImageResource(R.drawable.contact);
		}
		return convertView;
	}
	
	private class ViewHolder{
		ImageView imgAvatar;
		TextView tvNombre;
	}
}
