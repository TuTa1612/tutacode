package com.tutacode.addresbook;

import java.util.ArrayList;
import java.util.List;

import com.tutacode.R;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;

public class ContactsHelper {
	public interface ContactsListener{
		void GetAllContacts(List<Contacto>contactos);
		void GetContactDetail(Contacto unContacto);
	}
	
	public static boolean checkWhatsapp(Context ctx){
    	try {
			ctx.getPackageManager().getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
			return true;
		} catch (NameNotFoundException e) {
			return false;
		}
    }
	
	public static void callPhoneNumber(Context ctx, String number) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + number));
        ctx.startActivity(callIntent);
    }
    
	public static void sendSMS(Context ctx, String number){
    	Uri uri = Uri.parse("smsto:" + number);
    	Intent i = new Intent(Intent.ACTION_SENDTO, uri);
    	i.putExtra("sms_body", "TuTaCode");  
    	ctx.startActivity(i);
    }
    
	public static void sendWhatsapp(Context ctx, String number){
    	Uri uri = Uri.parse("smsto:" + number);
    	Intent i = new Intent(Intent.ACTION_SENDTO, uri);
    	i.putExtra("sms_body", "TuTaCode");  
    	i.setPackage("com.whatsapp");  
    	ctx.startActivity(i);
    	
    	/*Intent i = new Intent(Intent.ACTION_SENDTO, Uri.parse("content://com.android.contacts/data/" + mContacto.contact_ids.get(0)));
    	i.setType("text/plain");
    	i.setPackage("com.whatsapp");           // so that only Whatsapp reacts and not the chooser
    	i.putExtra(Intent.EXTRA_SUBJECT, "Subject");
    	i.putExtra(Intent.EXTRA_TEXT, "I'm the body.");
    	startActivity(i);*/
    	
    	/*TODO: SHARE WHATSAPP
    	 * Intent waIntent = new Intent(Intent.ACTION_SEND);
    	waIntent.setType("text/plain");
    	String text = "Sorry For Interruption,I'm Just Trying Something";
    	waIntent.setPackage("com.whatsapp");

    	if (waIntent != null) {
    	    waIntent.putExtra(Intent.EXTRA_TEXT, text);//
    	    waIntent.putExtra(Intent.EXTRA_SUBJECT, number);
    	    ctx.startActivity(Intent.createChooser(waIntent,"Send to friend"));
    	}*/
    }
    
	public static void sendEmail(Context ctx, String to){
    	Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",to, null));
    	emailIntent.putExtra(Intent.EXTRA_SUBJECT, ctx.getString(R.string.app_name));
    	ctx.startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }
	
	public void GetContactsFromAddresbookAsync(Context mContext, ContactsListener listener){
		new GetContactsFromAddresbookTask(listener).execute(mContext);
	}
	
	public void GetContactDetailFromAddresbookAsync(Context mContext, Contacto unContacto, ContactsListener listener){
		new GetContactDetailFromAddresbookTask(unContacto, listener).execute(mContext);
	}
	
	private class GetContactsFromAddresbookTask extends AsyncTask<Context, Float, List<Contacto>>{
		ContactsListener mListener;
		public GetContactsFromAddresbookTask(ContactsListener listener){
			mListener= listener;
		}
		
		protected List<Contacto> doInBackground(Context... arg0) {
			List<Contacto> TodosLosContactos = new ArrayList<Contacto>();
	    	ContentResolver cr = arg0[0].getContentResolver();
	        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? Contacts.DISPLAY_NAME_PRIMARY : Contacts.DISPLAY_NAME+" ASC");
	        
	        if(cursor.moveToFirst())
	        {
	            do
	            {
	                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
	                String nombre = cursor.getString(cursor.getColumnIndex(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? Contacts.DISPLAY_NAME_PRIMARY : Contacts.DISPLAY_NAME));
	                //String fotoMini = cursor.getString(cursor.getColumnIndex(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? Contacts.PHOTO_THUMBNAIL_URI : Contacts._ID));
	                String foto = cursor.getString(cursor.getColumnIndex(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? Contacts.PHOTO_URI : Contacts._ID));
	                Contacto unContacto = new Contacto();
	                unContacto.contact_ids.add(id);
	                unContacto.nombre = nombre;
	                unContacto.foto = foto;
	                
	                if (TodosLosContactos.size()== 0) {
	                	TodosLosContactos.add(unContacto);
					} else {
						Contacto lastContacto = TodosLosContactos.get(TodosLosContactos.size()-1);
						if(lastContacto.nombre.equals(unContacto.nombre)){
							if (!lastContacto.contact_ids.contains(unContacto.contact_ids.get(0))) {
									lastContacto.contact_ids.add(unContacto.contact_ids.get(0));
							}
		                	if (unContacto.foto!=null && unContacto.foto.trim().length()>0) {
								lastContacto.foto = unContacto.foto;
							}
		                }else{
		                	TodosLosContactos.add(unContacto);
		                }
					}	             
	            } while (cursor.moveToNext()) ;
	        }
	        return TodosLosContactos;
		}
		
		@Override
		protected void onPostExecute(List<Contacto> result) {
			mListener.GetAllContacts(result);
		}
	}
	
	private class GetContactDetailFromAddresbookTask extends AsyncTask<Context, Float, Contacto>{
		Contacto unContacto;
		ContactsListener mListener;
		
		public GetContactDetailFromAddresbookTask(Contacto pContacto, ContactsListener listener){
			unContacto = pContacto;
			mListener= listener;
		}
		
		protected Contacto doInBackground(Context... arg0) {
			ContentResolver cr = arg0[0].getContentResolver();
			
			for (String contactId : unContacto.contact_ids) {
				Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",new String[]{ contactId }, null);
                while (pCur.moveToNext()) 
                {
                    String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    if (!unContacto.telefonos.contains(contactNumber)) {
                    	unContacto.telefonos.add(contactNumber);
					}
                }
                pCur.close();
                
                Cursor mCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,null,ContactsContract.CommonDataKinds.Email.CONTACT_ID +" = ?",new String[]{ contactId }, null);
                while (mCur.moveToNext()) 
                {
                	String contactMail = mCur.getString(mCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                	if (!unContacto.emails.contains(contactMail)) {
                		unContacto.emails.add(contactMail);
                	}
                }
                mCur.close();
			}
	        return unContacto;
		}
		
		@Override
		protected void onPostExecute(Contacto result) {
			mListener.GetContactDetail(result);
		}
	}
}
