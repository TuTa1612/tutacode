package com.tutacode;

import java.util.HashMap;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.tutautils.core.HTTPUtils;
import com.tutautils.core.ILoading;
import com.tutautils.core.OnHTTPutilsResultListener;

public class FrgMapas extends SupportMapFragment{
	//HashMap<Marker, JsonResults> points;
	
	@Override
    public void onStart() {
    	super.onStart();
    	showMyPosition();
    	getPlaces("gas_station", R.drawable.gas_station);
    	
    	getMap().setOnMarkerClickListener(new OnMarkerClickListener() {
			
			@Override
			public boolean onMarkerClick(Marker marker) {
				CameraPosition posicion = CameraPosition.builder().target(marker.getPosition()).zoom(16).build();
		    	getMap().animateCamera(CameraUpdateFactory.newCameraPosition(posicion));
		    	Toast.makeText(getActivity(), marker.getTitle(), Toast.LENGTH_LONG).show();
		    	
		    	marker.showInfoWindow();
				return true;
			}
		});
    	
    	getMap().setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
			
			@Override
			public void onInfoWindowClick(Marker marker) {
		    	String to = String.valueOf(marker.getPosition().latitude)+","+String.valueOf(marker.getPosition().longitude);
		    	String uri = "http://maps.google.com/maps?saddr=&daddr="+to;
		    	Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
		    	intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
		    	startActivity(intent);
			}
		});
    }   
	     
	private void showMyPosition(){
		getMap().clear();
		Location location = ((ActivityMain)getActivity()).GetCurrentLocation();
		LatLng point = new LatLng(location.getLatitude(), location.getLongitude());
		
    	CameraPosition posicion = CameraPosition.builder().target(point).zoom(12).build();
    	getMap().animateCamera(CameraUpdateFactory.newCameraPosition(posicion));
    	
    	//BitmapDescriptor icono = BitmapDescriptorFactory.fromResource(R.drawable.ubicacion_assistcard);
    	MarkerOptions marker = new MarkerOptions().position(point);//.icon(icono);
    	getMap().addMarker(marker);
	}
	

     private void getPlaces(String type, final int icono){
    	 String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
    	 
    	 HashMap<String, Object> params = new HashMap<String, Object>();
    	 params.put("key", getString(R.string.keyGoogleApiPlaces));
    	 Location location = ((ActivityMain)getActivity()).GetCurrentLocation();
    	 String stringLocation = String.valueOf(location.getLatitude()) + "," + String.valueOf(location.getLongitude());
    	 params.put("location", stringLocation);
    	 params.put("radius", "5000");
    	 params.put("sensor", true);
    	 params.put("types", type);	
    	 
    	 ((ILoading)getActivity()).showLoading(true);
    	 OnHTTPutilsResultListener listener = new OnHTTPutilsResultListener() {
    		 @Override
    		 public void OnResult(Object resultado) {
    			 Gson gson = new Gson();
    			 showMyPosition();
    			 JsonPlaces results = gson.fromJson((String) resultado, JsonPlaces.class);
    			 
    			 //points = new HashMap<Marker, JsonResults>();
    			 for (JsonResults element : results.results) {
    				 MarkerOptions marker = new MarkerOptions().position(element.getLatLng());
    				 marker.title(element.name);
    				 marker.snippet(element.vicinity);
    				 marker.icon(BitmapDescriptorFactory.fromResource(icono));
        			 /*Marker oneMarker =*/ getMap().addMarker(marker);
        			 //points.put(oneMarker, element);
    			 }
    			 ((ILoading)getActivity()).hideLoading();
    		 }
    		 
    		 @Override
    		 public void OnFailed(Object motivo) {
    			 ((ILoading)getActivity()).hideLoading();
    			 Toast.makeText(getActivity(), R.string.mapas_Error, Toast.LENGTH_LONG).show();
    		 }
    	 };
	    	
    	 HTTPUtils http = new HTTPUtils();
    	 http.CallWSGetWithURLParamsAsync(url, params, listener); 
     }
     
     class JsonPlaces{
    	 public String next_page_token;
    	 public JsonResults[] results;
     }
     
     class JsonResults{
    	  public Point geometry;
    	  public String icon;
    	  public String id;
    	  public String name;
    	  public String reference;
    	  public String[] types;
    	  public String vicinity;
    	  
    	  public LatLng getLatLng(){
    		  LatLng point = new LatLng(geometry.location.lat, geometry.location.lng);
    		  return point;
    	  }
     }
     
     class Point{
    	 public PlaceLocation location;
     }
     
     class PlaceLocation{
    	 public float lat;
    	 public float lng;
     }
     
}
