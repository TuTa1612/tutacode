package com.tutacode.billing;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.tutacode.R;
import com.tutautils.core.ILoading;

public class FrgInterstitial extends Fragment{
	InterstitialAd interstitial;
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_interstitial, container, false);
		
		interstitial = new InterstitialAd(getActivity());
		interstitial.setAdUnitId(getString(R.string.interstitial_adunit_id));
		
		Button btnLoadInterstitial = (Button) view.findViewById(R.id.btnLoadInterstitial);
		btnLoadInterstitial.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				CargarInterstitial();
			}
		});
		
		return view;
	}
	
	private void CargarInterstitial(){
		AdRequest adRequest = new AdRequest.Builder().build();
		interstitial.setAdListener(mAdListener);
		interstitial.loadAd(adRequest);
		((ILoading)getActivity()).showLoading(false);
	}
	
	public void displayInterstitial() {
		((ILoading)getActivity()).hideLoading();
	    if (interstitial.isLoaded()) {
	      interstitial.show();
	    }
	  }

	AdListener mAdListener = new AdListener() {
		@Override
		public void onAdFailedToLoad(int errorCode) {
			super.onAdFailedToLoad(errorCode);
			((ILoading)getActivity()).hideLoading();
			Toast.makeText(getActivity(), R.string.ingresos_adfailed, Toast.LENGTH_LONG).show();
		}
		@Override
		public void onAdLoaded() {
			super.onAdLoaded();
			displayInterstitial();
		}	
	};
}
