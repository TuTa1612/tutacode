package com.tutacode.billing;

import java.util.ArrayList;
import java.util.List;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.vending.billing.IInAppBillingService;
import com.google.gson.Gson;
import com.tutacode.R;

public class FrgInAppPurchase extends Fragment{
	IInAppBillingService mService;
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_in_app_purchase, container, false);
		
		return view;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		StartServiceBilling(getActivity());
	}
	
	@Override
	public void onDestroy() {
    	super.onDestroy();
    	StopServiceBilling(getActivity());
    }
	
	ServiceConnection mServiceConn = new ServiceConnection() {
		@Override
		public void onServiceDisconnected(ComponentName name) {
			mService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mService = IInAppBillingService.Stub.asInterface(service);
			List<responseInAppItem> items = getAvailableItems(getActivity());
			Log.d("", "https://developer.android.com/google/play/billing/billing_integrate.html");
		}
	};
	
	//TODO: In App Billing
	private void StartServiceBilling(Context ctx){
		Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
		serviceIntent.setPackage("com.android.vending");
		ctx.bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
	}
		
	private void StopServiceBilling(Context ctx){
		if (mService != null) {
			ctx.unbindService(mServiceConn);
		} 
	}
	
	private List<responseInAppItem> getAvailableItems(Context ctx){
		List<responseInAppItem> InAppItems = new ArrayList<responseInAppItem>();
		
		ArrayList<String> skuList = new ArrayList<String> ();
		skuList.add("myduff001");
		skuList.add("coffeecup001");
		Bundle querySkus = new Bundle();
		querySkus.putStringArrayList("ITEM_ID_LIST", skuList);
		
		try {
			Bundle skuDetails = mService.getSkuDetails(3, ctx.getPackageName(), "inapp", querySkus);
			
			int response = skuDetails.getInt("RESPONSE_CODE");
			if (response == 0) {
			   ArrayList<String> responseList = skuDetails.getStringArrayList("DETAILS_LIST");
			   Gson gson = new Gson();
			   for (String thisResponse : responseList) {
				   InAppItems.add(gson.fromJson(thisResponse, responseInAppItem.class));
			   }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return InAppItems;
	}
}
