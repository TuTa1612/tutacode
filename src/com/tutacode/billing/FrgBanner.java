package com.tutacode.billing;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.tutacode.R;
import com.tutautils.core.ILoading;

public class FrgBanner extends Fragment{
	AdView banner;
	Button btnLoadBanner;
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_banner, container, false);
		banner = (AdView) view.findViewById(R.id.adView);
		btnLoadBanner = (Button) view.findViewById(R.id.btnLoadBanner);
		btnLoadBanner.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				btnLoadBanner.setVisibility(View.GONE);
				CargarBanner();
			}
		});
		
		
		return view;
	}
	
	private void CargarBanner(){
		AdRequest adRequest = new AdRequest.Builder().build();
		banner.setAdListener(mAdListener);
		((ILoading)getActivity()).showLoading(false);
		banner.loadAd(adRequest);
	}
	
	AdListener mAdListener = new AdListener() {
		@Override
		public void onAdFailedToLoad(int errorCode) {
			super.onAdFailedToLoad(errorCode);
			((ILoading)getActivity()).hideLoading();
			Toast.makeText(getActivity(), R.string.ingresos_adfailed, Toast.LENGTH_LONG).show();
		}
		@Override
		public void onAdLoaded() {
			super.onAdLoaded();
			((ILoading)getActivity()).hideLoading();
		}	
	};
}
