package com.tutacode;

import java.io.IOException;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;

import com.facebook.AppEventsLogger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class ActivityPreApp extends FragmentActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
	/*private static final String TWITTER_KEY = "lOBUEx2eKkeL0YdFtBaeszmn8";
	private static final String TWITTER_SECRET = "B3jgEGUKkGkwrh1PvGlSXQcMLzvwj5AZCtlwV3IoqqdJTZvVS9";
	*/
	//PUSH NOTIFICATIONS
		GoogleCloudMessaging gcm;
		String regid;
		public static final String GCM_SHARED_PREFERENCES = "GCMSharedPreferences";
	    public static final String PROPERTY_REG_ID = "registration_id";
	    private static final String PROPERTY_APP_VERSION = "appVersion";
	    //TODO: check this project ID in developer console
	    String SENDER_ID = "384807336968";

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preapp);
        getSupportFragmentManager().beginTransaction().replace(R.id.flPreappContainer, new FrgSplash()).commit();
        new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				/*FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
				fragmentTransaction.setTransition(android.R.anim.bounce_interpolator);
				fragmentTransaction.replace(R.id.flPreappContainer, new FrgLoginServices());
				fragmentTransaction.commit();*/
				Intent i = new Intent(ActivityPreApp.this, ActivityMain.class);
				startActivity(i);
			}
		}, 5000);
        registerToGCMPush();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	AppEventsLogger.activateApp(this);
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    	AppEventsLogger.deactivateApp(this);
    }
    
    public boolean registerToGCMPush(){
		int result = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if(result == ConnectionResult.SUCCESS){
        	gcm = GoogleCloudMessaging.getInstance(this);
            regid = getStoredRegistrationId(ActivityPreApp.this);

            if (regid.isEmpty()) {
                registerInBackground(ActivityPreApp.this);
            }
        	return true;
        }else{
        	GooglePlayServicesUtil.getErrorDialog(result, ActivityPreApp.this, 0).show();
        	return false;
        }
	}
    
    public static String getStoredRegistrationId(Context ctx) {
	    final SharedPreferences prefs = ctx.getSharedPreferences(GCM_SHARED_PREFERENCES, Context.MODE_PRIVATE);
	    String registrationId = prefs.getString(PROPERTY_REG_ID, "");
	    if (registrationId.isEmpty()) {
	        Log.i("GCM PUSH", "Registration not found.");
	        return "";
	    }
	    // Check if app was updated; if so, it must clear the registration ID
	    // since the existing regID is not guaranteed to work with the new
	    // app version.
	    int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
	    int currentVersion = getAppVersion(ctx);
	    if (registeredVersion != currentVersion) {
	        Log.i("GCM PUSH", "App version changed.");
	        return "";
	    }
	    return registrationId;
	}
    
    private void storeRegistrationId(Context context, String regId) {
	    final SharedPreferences prefs = getSharedPreferences(GCM_SHARED_PREFERENCES, Context.MODE_PRIVATE);
	    int appVersion = getAppVersion(context);
	    Log.i("GCM PUSH", "Saving regId on app version " + appVersion);
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putString(PROPERTY_REG_ID, regId);
	    editor.putInt(PROPERTY_APP_VERSION, appVersion);
	    editor.commit();
	}
	
	private static int getAppVersion(Context ctx) {
	    try {
	        PackageInfo packageInfo = ctx.getPackageManager()
	                .getPackageInfo(ctx.getPackageName(), 0);
	        return packageInfo.versionCode;
	    } catch (NameNotFoundException e) {
	        throw new RuntimeException("Could not get package name: " + e);
	    }
	}
	
	private void registerInBackground(final Context ctx) {
	    new AsyncTask() {
	        @Override
	        protected String doInBackground(Object... params) {
	            String msg = "";
	            try {
	                if (gcm == null) {
	                    gcm = GoogleCloudMessaging.getInstance(ctx);
	                }
	                regid = gcm.register(SENDER_ID);
	                msg = "Device registered, registration ID=" + regid;

	                // You should send the registration ID to your server over HTTP,
	                // so it can use GCM/HTTP or CCS to send messages to your app.
	                // The request to your server should be authenticated if your app
	                // is using accounts.
	                sendRegistrationIdToBackend(regid);

	                // For this demo: we don't need to send it because the device
	                // will send upstream messages to a server that echo back the
	                // message using the 'from' address in the message.

	                // Persist the regID - no need to register again.
	                storeRegistrationId(ctx, regid);
	            } catch (IOException ex) {
	                msg = "Error :" + ex.getMessage();
	                // If there is an error, don't just keep trying to register.
	                // Require the user to click a button again, or perform
	                // exponential back-off.
	            }
	            return msg;
	        }

	    }.execute(null, null, null);
	}
	
	private void sendRegistrationIdToBackend(String regid) {
	    // Your implementation here.
	}
}
