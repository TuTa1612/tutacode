package com.tutacode;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DrawerAdapter extends BaseAdapter{
	private Context mContext;
	private List<DrawerItem> mItems;
	
	public DrawerAdapter(Context ctx){
		mContext = ctx;
		mItems = createDrawerItems();
	}
	
	@Override
	public int getCount() {
		return mItems != null ? mItems.size() : 0;
	}

	@Override
	public DrawerItem getItem(int position) {
		return position >= 0 && position<getCount() ? mItems.get(position) : null;
	}

	@Override
	public long getItemId(int position) {return 0;}
	

	private class ViewHolderDrawerItem{
		public ImageView ivDrawerIcon;
		public TextView tvDrawerText;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolderDrawerItem viewHolder;
		if (convertView==null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.drawer_list_item, parent, isEmpty());
			viewHolder = new ViewHolderDrawerItem();
			viewHolder.ivDrawerIcon = (ImageView) convertView.findViewById(R.id.ivDrawerItemIcon);
			viewHolder.tvDrawerText = (TextView) convertView.findViewById(R.id.tvDrawerItemTitle);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolderDrawerItem) convertView.getTag();
		}
		
		DrawerItem item = getItem(position);
		if(item!=null){
			viewHolder.ivDrawerIcon.setImageResource(item.resImg);
			viewHolder.tvDrawerText.setText(item.nombre);
		}
		
		return convertView;
	}

	
	/*new ArrayAdapter<String>(this, R.layout.drawer_list_item, 
    		getResources().getStringArray(R.array.draweritems))*/
	
	private List<DrawerItem> createDrawerItems() {
		List<DrawerItem> draweritems = new ArrayList<DrawerAdapter.DrawerItem>();
		
		DrawerItem itemSocial = new  DrawerItem();
		itemSocial.resImg = R.drawable.ic_launcher;
		itemSocial.nombre = mContext.getString(R.string.drawer_item_social);
		itemSocial.frgmnt = new FrgSocial();
		draweritems.add(itemSocial);
		
		DrawerItem itemAgenda = new  DrawerItem();
		itemAgenda.resImg = R.drawable.ic_launcher;
		itemAgenda.nombre = mContext.getString(R.string.drawer_item_agenda);
		itemAgenda.frgmnt = new FrgAgenda();
		draweritems.add(itemAgenda);
		
		DrawerItem itemStreaming = new  DrawerItem();
		itemStreaming.resImg = R.drawable.ic_launcher;
		itemStreaming.nombre = mContext.getString(R.string.drawer_item_streaming);
		itemStreaming.frgmnt = new FrgStreaming();
		draweritems.add(itemStreaming);
		
		DrawerItem itemMapas = new  DrawerItem();
		itemMapas.resImg = R.drawable.ic_launcher;
		itemMapas.nombre = mContext.getString(R.string.drawer_item_mapas);
		itemMapas.frgmnt = new FrgMapas();
		draweritems.add(itemMapas);
		
		DrawerItem itemIngresos = new  DrawerItem();
		itemIngresos.resImg = R.drawable.ic_launcher;
		itemIngresos.nombre = mContext.getString(R.string.drawer_item_ingresos);
		itemIngresos.frgmnt = new FrgIngresos();
		draweritems.add(itemIngresos);
		
		DrawerItem itemUploadPhoto = new  DrawerItem();
		itemUploadPhoto.resImg = R.drawable.ic_launcher;
		itemUploadPhoto.nombre = mContext.getString(R.string.drawer_item_uploadPhoto);
		itemUploadPhoto.frgmnt = new FrgUploadPhoto();
		draweritems.add(itemUploadPhoto);
		
		DrawerItem itemApiRestLogin = new  DrawerItem();
		itemApiRestLogin.resImg = R.drawable.ic_launcher;
		itemApiRestLogin.nombre = mContext.getString(R.string.drawer_item_apirest_login);
		itemApiRestLogin.frgmnt = new FrgApiRestLogIn();
		draweritems.add(itemApiRestLogin);
		
		DrawerItem itemCV = new  DrawerItem();
		itemCV.resImg = R.drawable.ic_launcher;
		itemCV.nombre = mContext.getString(R.string.drawer_item_curriculumvitae);
		itemCV.frgmnt = new FrgCurriculumVitae();
		draweritems.add(itemCV);
		
		return draweritems;
	}
	
	public class DrawerItem{
		public int resImg;
		public String nombre;
		public Fragment frgmnt;
	}
}
