package com.tutacode;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.tutacode.addresbook.AdptrAgenda;
import com.tutacode.addresbook.Contacto;
import com.tutacode.addresbook.ContactsHelper;
import com.tutacode.addresbook.ContactsHelper.ContactsListener;
import com.tutautils.core.ILoading;

public class FrgAgenda extends Fragment implements OnItemClickListener{
	ListView listadoContactos;
	AdptrAgenda adapter;
	ContactsHelper helper;
	
    public FrgAgenda() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_agenda, container, false);
        listadoContactos = (ListView) rootView.findViewById(R.id.lstContactos);
        listadoContactos.setOnItemClickListener(this);
        if(adapter==null)
        	adapter = new AdptrAgenda(getActivity());
        listadoContactos.setAdapter(adapter);
        return rootView;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	helper = new ContactsHelper();
    	LoadContactsFromAddresBook();
    }
    
    ContactsListener contactsListener = new ContactsListener() {
		
		@Override
		public void GetContactDetail(Contacto unContacto) {
			new AccionesDialog(unContacto).show(getFragmentManager(), "ACCIONES");
			((ILoading)getActivity()).hideLoading();
		}
		
		@Override
		public void GetAllContacts(List<Contacto> contactos) {
			adapter.setSource(contactos);
			((ILoading)getActivity()).hideLoading();
		}
	};
    
    public void LoadContactsFromAddresBook(){
    	((ILoading)getActivity()).showLoading(false);
    	helper.GetContactsFromAddresbookAsync(getActivity(), contactsListener);
    }

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		((ILoading)getActivity()).showLoading(false);
		helper.GetContactDetailFromAddresbookAsync(getActivity(), adapter.getItem(arg2), contactsListener);
	}

	
	@SuppressLint("ValidFragment")
	public class AccionesDialog extends DialogFragment{
		Contacto mContacto;
		public AccionesDialog(Contacto unContacto){
			mContacto = unContacto;
		}
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View viewDialog = inflater.inflate(R.layout.contacto_acciones, container);
			LinearLayout llTelefonos = (LinearLayout) viewDialog.findViewById(R.id.llDetailsTelefonos);
	        LinearLayout llEmails = (LinearLayout) viewDialog.findViewById(R.id.llDetailsEmails);
			
			for (String tel : mContacto.telefonos) {
				llTelefonos.addView(getPhoneTextView(getActivity(), tel));
			}
	        for (String mail : mContacto.emails) {
				llEmails.addView(getEmailTextView(getActivity(), mail));
			}
	        
			getDialog().setTitle(mContacto.nombre);
			return viewDialog;
		}
		
		private View getPhoneTextView(final Context ctx, String onePhone){
			View view = LayoutInflater.from(ctx).inflate(R.layout.contacto_acciones_item, null);
			ImageView img = (ImageView) view.findViewById(R.id.imgContactoAccionesItem);
			ImageView img2 = (ImageView) view.findViewById(R.id.imgContactoAccionesItem2);
			ImageView img3 = (ImageView) view.findViewById(R.id.imgContactoAccionesItem3);
			final TextView tv = (TextView) view.findViewById(R.id.tvContactoAccionesItem);

			img.setImageResource(R.drawable.phone);
			tv.setText(onePhone);
			tv.setTag(onePhone);
			view.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					ContactsHelper.callPhoneNumber(ctx, tv.getTag().toString());
				}
			});
			
			img2.setImageResource(R.drawable.mail);
			img2.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					ContactsHelper.sendSMS(ctx, tv.getTag().toString());
				}
			});
			img2.setVisibility(View.VISIBLE);
			
			if (ContactsHelper.checkWhatsapp(ctx)) {
				img3.setImageResource(R.drawable.whatsapp);
				img3.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						ContactsHelper.sendWhatsapp(ctx, tv.getTag().toString());
					}
				});
				img3.setVisibility(View.VISIBLE);
			} else {
				img3.setVisibility(View.GONE);
			}
			
			return view;
	    }
	    private View getEmailTextView(final Context ctx, String oneEmail){
	    	View view = LayoutInflater.from(ctx).inflate(R.layout.contacto_acciones_item, null);
			ImageView img = (ImageView) view.findViewById(R.id.imgContactoAccionesItem);
			final TextView tv = (TextView) view.findViewById(R.id.tvContactoAccionesItem);
			
			img.setImageResource(R.drawable.mail);
			tv.setText(oneEmail);
			tv.setTag(oneEmail);
			view.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					ContactsHelper.sendEmail(ctx, tv.getTag().toString());
				}
			});
			return view;
	    }
	    
	    
	}
}