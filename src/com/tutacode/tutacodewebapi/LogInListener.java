package com.tutacode.tutacodewebapi;

public interface LogInListener{
	void LogIN_OK(UserBackend unUserBackend);
	void LogIN_Failed();
}