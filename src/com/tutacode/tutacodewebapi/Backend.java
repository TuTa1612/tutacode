package com.tutacode.tutacodewebapi;

import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.tutautils.core.HTTPUtils;
import com.tutautils.core.OnHTTPutilsResultListener;
import com.tutautils.core.Utils;

public class Backend {
	private final String urlWebApi = "http://tutacode.azurewebsites.net/api/";
	private HTTPUtils httpUtils;
	
	//Singleton
	private static Backend instance;
	private Backend(){
		httpUtils = new HTTPUtils();
	}
	public static Backend GetInstance(){
		if(instance == null){
			instance = new Backend();
		}
		return instance;
	}
	
	private static final String USERBACKEND_SHARED_PREFERENCES = "UserBackendSP";
	private static final String KEY_USERGUID = "keyUserGuid";
	private static final String KEY_USERNICK = "keyUserNick";
	private static final String KEY_USERMAIL = "keyUserMail";
	public UserBackend getStoredUser(Context ctx) {
	    final SharedPreferences prefs = ctx.getSharedPreferences(USERBACKEND_SHARED_PREFERENCES, Context.MODE_PRIVATE);
	    UserBackend unUserBackend = new UserBackend();
	    unUserBackend.UserGuid = prefs.getString(KEY_USERGUID, "");
	    unUserBackend.UserNick = prefs.getString(KEY_USERNICK, "");
	    unUserBackend.UserMail = prefs.getString(KEY_USERMAIL, "");
	    
	    if(Utils.isNullOrEmpty(unUserBackend.UserGuid)
	    	&& Utils.isNullOrEmpty(unUserBackend.UserNick)
	    	&& Utils.isNullOrEmpty(unUserBackend.UserMail)){
	    	return null;
	    }
	    return unUserBackend;
	}
    
    private void saveUserBackend(Context ctx, UserBackend unUserBackend) {
    	final SharedPreferences prefs = ctx.getSharedPreferences(USERBACKEND_SHARED_PREFERENCES, Context.MODE_PRIVATE);
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putString(KEY_USERGUID, unUserBackend!=null ? unUserBackend.UserGuid : null);
	    editor.putString(KEY_USERNICK, unUserBackend!=null ? unUserBackend.UserNick : null);
	    editor.putString(KEY_USERMAIL, unUserBackend!=null ? unUserBackend.UserMail : null);
	    editor.commit();
	}
    
    //LOG IN
    private class LoginResponse{
    	public boolean Status;
    	public String ErrorMsg;
    	public UserBackend Body;
    }
    public void LoginAsync(final Context ctx, String userMail, String userPassword, String userGCMToken, final LogInListener listener){
    	HashMap<String, Object> params = new HashMap<String, Object>();
    	params.put("UserMail", userMail);
    	params.put("UserPassword", userPassword);
    	params.put("UserGCMToken", userGCMToken);
    	
    	httpUtils.CallWSPostWithFormParamsAsync(urlWebApi+"LogIn", params, new OnHTTPutilsResultListener() {
			
			@Override
			public void OnResult(Object resultado) {
				Gson gson = new Gson();
				LoginResponse response = gson.fromJson((String)resultado, LoginResponse.class);
				if(response.Status){
					saveUserBackend(ctx, response.Body);
					listener.LogIN_OK(response.Body);
				}else{
					listener.LogIN_Failed();
				}
			}
			
			@Override
			public void OnFailed(Object motivo) {
				listener.LogIN_Failed();
			}
		});
    }
    
    //LOG OUT
    private class LogoutResponse{
    	public boolean Status;
    	public String ErrorMsg;
    	public Object Body;
    }
    public void LogOutAsync(final Context ctx, String userGuid, final LogOutListener listener){
    	HashMap<String, Object> params = new HashMap<String, Object>();
    	params.put("UserGuid", userGuid);
    	httpUtils.CallWSPostWithFormParamsAsync(urlWebApi+"LogOut", params, new OnHTTPutilsResultListener() {
			
			@Override
			public void OnResult(Object resultado) {
				Gson gson = new Gson();
				LogoutResponse response = gson.fromJson((String)resultado, LogoutResponse.class);
				if(response.Status){
					saveUserBackend(ctx, null);
					listener.LogOUT_OK();
				}else{
					listener.LogOUT_Failed();
				}
			}
			
			@Override
			public void OnFailed(Object motivo) {
				listener.LogOUT_Failed();
			}
		});
    }
    
    public void SignUpAsync(){}
}

