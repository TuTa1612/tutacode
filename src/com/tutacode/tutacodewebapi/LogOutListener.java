package com.tutacode.tutacodewebapi;

public interface LogOutListener{
	void LogOUT_OK();
	void LogOUT_Failed();
}