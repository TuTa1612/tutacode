package com.tutacode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

import com.facebook.android.Util;
import com.tutautils.core.HTTPUtils;
import com.tutautils.core.ILoading;
import com.tutautils.core.OnHTTPutilsResultListener;
import com.tutautils.core.Utils;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;

public class FrgUploadPhoto extends Fragment{
	static Uri capturedImageUri=null;
	public final static int CAMERA_RESULT = 2506;
	
	EditText etNombreFoto;
	ImageView ivImgCapturada;
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_uploadphoto, container, false);
		etNombreFoto = (EditText) view.findViewById(R.id.etNombreFoto);
		ivImgCapturada = (ImageView) view.findViewById(R.id.ivImgCapturada);
		Button btnCamara = (Button) view.findViewById(R.id.btnTakePicture);
		Button btnUpload = (Button) view.findViewById(R.id.btnUploadPicture);
		
		btnCamara.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View v) {
	        	takePicture();
	       }
	    });
		
		btnUpload.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				uploadPicture();
			}
		});
	
		return view;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAMERA_RESULT) {  
	        //Bitmap photo = (Bitmap) data.getExtras().get("data");
	        //imageView.setImageBitmap(photo);
	        try {
	        	Bitmap bitmap = MediaStore.Images.Media.getBitmap( getActivity().getApplicationContext().getContentResolver(),  capturedImageUri);
	        	ivImgCapturada.setImageBitmap(bitmap);
	        } catch (FileNotFoundException e) {
	        	// TODO Auto-generated catch block
	        	e.printStackTrace();
	        } catch (IOException e) {
	        	// TODO Auto-generated catch block
	        	e.printStackTrace();
	        }
	    }  
	}
	
	private void takePicture(){
		if(!Utils.isValidEditText(etNombreFoto)){
    		return;
    	}
		File file = new File(Environment.getExternalStorageDirectory(),  (etNombreFoto.getText().toString()));
    	if(file.exists()){
    		file.delete();
    	}
    	try {
    		file.createNewFile();
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	capturedImageUri = Uri.fromFile(file);
	    Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
	    i.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageUri);
	    startActivityForResult(i, CAMERA_RESULT);
	}
	
	private void uploadPicture(){
		if(Utils.isNullOrEmpty(capturedImageUri.toString()) || ivImgCapturada.getDrawable() == null){
    		return;
    	}
		
		HTTPUtils http = new HTTPUtils();
		
		String url = "http://www.tutacode.somee.com/api/test01";
		
		/*HashMap<String, Object> urlParams = new HashMap<String, Object>();
		urlParams.put("idObj", 11);
		urlParams.put("Nombre", "TuTa");*/
		
		MultipartEntity multipartParams = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		try{
			multipartParams.addPart("Title", new StringBody("Title"));
			File oneFile = new File(capturedImageUri.getPath());
			multipartParams.addPart("file1", new FileBody(oneFile));
		} catch (Exception e) {
			Log.d("Request", "Error al crear los paramertos");
		}	
		
		((ILoading)getActivity()).showLoading(false);
		http.CallWSPostWithURLParamsAndMultipartParams(url, null, multipartParams, new OnHTTPutilsResultListener() {
			
			@Override
			public void OnResult(Object resultado) {
				Toast.makeText(getActivity(), (String) resultado, Toast.LENGTH_LONG).show();
				((ILoading)getActivity()).hideLoading();
			}
			
			@Override
			public void OnFailed(Object motivo) {
				Toast.makeText(getActivity(), "FAILED", Toast.LENGTH_LONG).show();
				((ILoading)getActivity()).hideLoading();
			}
		});
	}

}
