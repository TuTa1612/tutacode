package com.tutacode;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.AppEventsLogger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;
import com.tutacode.DrawerAdapter.DrawerItem;
import com.tutautils.core.GPSListener;
import com.tutautils.core.ILoading;


public class ActivityMain extends ActionBarActivity	implements ILoading{
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private CharSequence mTitle;
	private CharSequence mDrawerTitle;
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerAdapter adapter;
	private ProgressDialog loadingDialog;
	private boolean doubleBackToExitPressedOnce;
	GoogleApiClient mGoogleApiClient;

    @SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mTitle = mDrawerTitle = getTitle();

        // Set the adapter for the list view
        adapter = new DrawerAdapter(getApplicationContext());
        mDrawerList.setAdapter(adapter);
        
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        
        
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        
        loadingDialog = new ProgressDialog(ActivityMain.this);
        //TODO: Check Google Play Services
        //new GPSHelper(ActivityMain.this, gpsListener, 0, false);
        int result = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if(result == ConnectionResult.SUCCESS){
        	mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
        							.addApi(LocationServices.API).addConnectionCallbacks(apiclientCallbacks)
        							.addOnConnectionFailedListener(apiclientOnFailed)
        							.build();
        	
        }else{
        	GooglePlayServicesUtil.getErrorDialog(result, ActivityMain.this, 0).show();
        }
    }
    
    ///TODO:Google Api Client
    private static Location lastLocation;
    public Location GetCurrentLocation(){
    	Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    	if (currentLocation != null) {
    		lastLocation = currentLocation;
		}
    	return lastLocation;
    }
    ConnectionCallbacks apiclientCallbacks = new ConnectionCallbacks() {
		@Override
		public void onConnectionSuspended(int cause) {
			
		}
		@Override
		public void onConnected(Bundle connectionHint) {
			Location mLocation = GetCurrentLocation();
		}
	};
	OnConnectionFailedListener apiclientOnFailed = new OnConnectionFailedListener() {
		@Override
		public void onConnectionFailed(ConnectionResult result) {
			
		}
	};
	@Override
	protected void onStart() {
		super.onStart();
		mGoogleApiClient.connect();
	};
	@Override
	protected void onStop() {
		mGoogleApiClient.disconnect();
		super.onStop();
	}
	

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        //boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        //menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
          return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	AppEventsLogger.activateApp(this);
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    	AppEventsLogger.deactivateApp(this);
    }
    
    
    //DRAWER
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            selectItem(position);
        }

    }
    
    /** Swaps fragments in the main content view */
    private void selectItem(int position) {
        DrawerItem item = adapter.getItem(position);
        mDrawerList.setItemChecked(position, true);
        openFragment(item.frgmnt, false);
        setTitle(item.nombre);
    }
    
    public void openFragment(Fragment oneFragment, boolean addToBackStack){
        FragmentTransaction frgTransaction = getSupportFragmentManager().beginTransaction();
        frgTransaction.replace(R.id.flMainContainer, oneFragment);
        if(addToBackStack){
        	frgTransaction.addToBackStack(oneFragment.getClass().getName());
        }else{
        	getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        frgTransaction.commit();
        mDrawerLayout.closeDrawer(mDrawerList);
    }
    
    @Override
    public void onBackPressed() {
    	if (getSupportFragmentManager().getBackStackEntryCount()==0 && !doubleBackToExitPressedOnce) {
    		doubleBackToExitPressedOnce = true;
    		Toast.makeText(ActivityMain.this, R.string.back_to_exit, Toast.LENGTH_SHORT).show();
    		new Handler().postDelayed(new Runnable() {
    	        @Override
    	        public void run() {
    	            doubleBackToExitPressedOnce=false;                       
    	        }
    	    }, 2000);
			return;
		}
    	
		
    	super.onBackPressed();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        if (Build.VERSION.SDK_INT<11) {
			setTitle(title);
		} else {
			getSupportActionBar().setTitle(mTitle);
		}
    }

	@Override
	public void showLoading(boolean cancelable, String mensaje) {
		if(loadingDialog!=null){
			loadingDialog.setCancelable(cancelable);
			loadingDialog.setMessage(mensaje);
			loadingDialog.show();
		}
	}

	@Override
	public void showLoading(boolean cancelable) {
		showLoading(cancelable, getString(R.string.loading_default));	
	}

	@Override
	public void hideLoading() {
		if(loadingDialog!=null)
			loadingDialog.dismiss();
	}
	
	private GPSListener gpsListener = new GPSListener() {
		
		@Override
		public void PedirGPS() {
			AlertDialog.Builder builder = new Builder(ActivityMain.this);
			builder.setCancelable(true).setTitle(R.string.pedirgps_titulo)
			.setPositiveButton(R.string.pedirgps_aceptar, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					Intent gpsOptionsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);  
					startActivity(gpsOptionsIntent);
				}
			}).setNegativeButton(R.string.pedirgps_cancelar, null);
			
			AlertDialog dialog = builder.create();
			dialog.show();

		}
		
		@Override
		public void ActualizarUbicacion(float latitud, float longitud) {
			//Toast.makeText(ActivityMain.this, "Latitud: " + String.valueOf(latitud) + "\nLongitud: "+ String.valueOf(longitud), Toast.LENGTH_LONG).show();
		}
	};
}
