package com.tutacode;

import com.tutacode.social.FrgFacebook;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

public class FrgSocial extends Fragment{
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_social, container, false);
		
		Button btnFacebook = (Button) view.findViewById(R.id.btnFacebookServices);
		btnFacebook.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((ActivityMain)getActivity()).openFragment(new FrgFacebook(), true);
			}
		});
		
		Button btnTwitter = (Button) view.findViewById(R.id.btnTwitterServices);
		btnTwitter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(getActivity(), R.string.feature_indevelopment, Toast.LENGTH_SHORT).show();
			}
		});
		
		Button btnGooglePlus = (Button) view.findViewById(R.id.btnGooglePlusServices);
		btnGooglePlus.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(getActivity(), R.string.feature_indevelopment, Toast.LENGTH_SHORT).show();
			}
		});
		
		Button btnLinkedIn = (Button) view.findViewById(R.id.btnLinkedInsServices);
		btnLinkedIn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(getActivity(), R.string.feature_indevelopment, Toast.LENGTH_SHORT).show();
			}
		});
		
		return view;
	}
}
