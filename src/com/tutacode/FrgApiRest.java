package com.tutacode;

import com.tutacode.tutacodewebapi.Backend;
import com.tutacode.tutacodewebapi.LogInListener;
import com.tutacode.tutacodewebapi.LogOutListener;
import com.tutacode.tutacodewebapi.UserBackend;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class FrgApiRest extends Fragment{
	TextView tvUserName;
	Button btnLogIn;
	Button btnLogOut;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_apirest_notas, container, false);
        tvUserName = (TextView) rootView.findViewById(R.id.tvUserName);
        btnLogIn = (Button) rootView.findViewById(R.id.btnLogIn);
        btnLogOut = (Button) rootView.findViewById(R.id.btnLogOut);
        
        btnLogIn.setOnClickListener(btnLogin_click);
        btnLogOut.setOnClickListener(btnLogOut_click);
        RefreshUI();
        return rootView;
    }
	
	OnClickListener btnLogin_click = new OnClickListener() {
		@Override
		public void onClick(View v) {
			String userMail = "user1@gmail.com";
			String userPassword = "password1";
			String userGCMToken = ActivityPreApp.getStoredRegistrationId(getActivity());
			Backend.GetInstance().LoginAsync(getActivity(), userMail, userPassword, userGCMToken, new LogInListener() {
				
				@Override
				public void LogIN_OK(UserBackend unUserBackend) {
					RefreshUI();
				}
				
				@Override
				public void LogIN_Failed() {
					Toast.makeText(getActivity(), "LOG IN FAILED", Toast.LENGTH_SHORT).show();
				}
			});
		}
	};
	
	OnClickListener btnLogOut_click = new OnClickListener() {
		@Override
		public void onClick(View v) {
			String userGuid = Backend.GetInstance().getStoredUser(getActivity()).UserGuid;
			Backend.GetInstance().LogOutAsync(getActivity(), userGuid, new LogOutListener() {
				
				@Override
				public void LogOUT_OK() {
					RefreshUI();
				}
				
				@Override
				public void LogOUT_Failed() {
					Toast.makeText(getActivity(), "LOG OUT FAILED", Toast.LENGTH_SHORT).show();
				}
			});
		}
	};
	
	private void RefreshUI(){
		if (Backend.GetInstance().getStoredUser(getActivity()) != null) {
			tvUserName.setVisibility(View.VISIBLE);
			btnLogIn.setVisibility(View.GONE);
			btnLogOut.setVisibility(View.VISIBLE);
			tvUserName.setText(Backend.GetInstance().getStoredUser(getActivity()).UserNick);
		} else {
			tvUserName.setVisibility(View.GONE);
			btnLogIn.setVisibility(View.VISIBLE);
			btnLogOut.setVisibility(View.GONE);
		}
	}
}

