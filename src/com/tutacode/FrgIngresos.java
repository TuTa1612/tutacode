package com.tutacode;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.tutacode.billing.FrgBanner;
import com.tutacode.billing.FrgInAppPurchase;
import com.tutacode.billing.FrgInterstitial;

public class FrgIngresos extends Fragment{
	/*"https://developer.android.com/google/play/billing/billing_integrate.html#billing-requests"
	"https://developer.android.com/google/play/billing/billing_best_practices.html"
	"https://developer.android.com/google/play/billing/billing_reference.html"
	"https://developer.android.com/google/play/billing/api.html"
	"https://developer.android.com/google/play/billing/billing_overview.html"
	"https://developer.android.com/google/play/billing/index.html?hl=d"*/
	
	//https://apps.admob.com/#home
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_ingresos, container, false);
		
		Button btnInAppBilling = (Button) view.findViewById(R.id.btnInAppBilling);
		btnInAppBilling.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((ActivityMain)getActivity()).openFragment(new FrgInAppPurchase(), true);
			}
		});
		
		Button btnBannerStandard = (Button) view.findViewById(R.id.btnBannerStandard);
		btnBannerStandard.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((ActivityMain)getActivity()).openFragment(new FrgBanner(), true);
			}
		});
		
		Button btnInterstitialAd  = (Button) view.findViewById(R.id.btnInterstitialAd);
		btnInterstitialAd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((ActivityMain)getActivity()).openFragment(new FrgInterstitial(), true);
			}
		});
		
		return view;
	}
}
