package com.tutacode;

import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tutacode.addresbook.ContactsHelper;
import com.tutautils.core.HTTPUtils;
import com.tutautils.core.ILoading;
import com.tutautils.core.OnHTTPutilsResultListener;
import com.tutautils.core.Utils;

public class FrgCurriculumVitae extends Fragment{
	private final String URL_CV = "https://docs.google.com/document/d/17w0HQswHEB3sd2IzejR8zjBAmdtUI_7FAU7eg0WPdU0/mobilebasic";
	private final String MAIL_DEVELOPER = "lucassaavedra@yahoo.com.ar";
	EditText etMailTo;
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_curriculum, container, false);
		Button btnCVOnline = (Button) view.findViewById(R.id.btnVerCVOnline);
		etMailTo = (EditText) view.findViewById(R.id.etMailTo);
		Button btnSendMail = (Button) view.findViewById(R.id.btnEnviarCVporMail);
		Button btnMailDeveloper = (Button) view.findViewById(R.id.btnEnviarMailAlDesarrollador);
		
		btnCVOnline.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				LoadCVInBrowser();
			}
		});
		
		btnSendMail.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SendCVByEmail(etMailTo.getText().toString());
			}
		});
		
		btnMailDeveloper.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SendMailToDeveloper();
			}
		});
		
		return view;
	}
	
	private void OcultarTeclado(){
		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(etMailTo.getWindowToken(), 0);
	}
	private void LoadCVInBrowser(){
		OcultarTeclado();
		Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse(URL_CV));
		startActivity(intent);
	}
	
	private void SendCVByEmail(String to){
		if(!Utils.isValidEmail(to)){
			Toast.makeText(getActivity(), R.string.cv_send_to_mail_invalid, Toast.LENGTH_SHORT).show();
			return;
		}
		OcultarTeclado();
		((ILoading)getActivity()).showLoading(false);
		String url = "https://script.google.com/macros/s/AKfycbzadb9lXc2mIBKqX9rvw04LmZmJ7sIKE2tMxGMFlRCET017uvw/exec?";
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("action", "sendMailTo");
		params.put("to", to);
		new HTTPUtils().CallWSGetWithURLParamsAsync(url, params, new OnHTTPutilsResultListener() {
			
			@Override
			public void OnResult(Object resultado) {
				((ILoading)getActivity()).hideLoading();
				etMailTo.setText(null);
				Toast.makeText(getActivity(), R.string.cv_send_to_mail_ok, Toast.LENGTH_SHORT).show();
			}
			
			@Override
			public void OnFailed(Object motivo) {
				((ILoading)getActivity()).hideLoading();
				Toast.makeText(getActivity(), R.string.cv_send_to_mail_failed, Toast.LENGTH_SHORT).show();
			}
		});
	}
	
	private void SendMailToDeveloper(){
		OcultarTeclado();
		ContactsHelper.sendEmail(getActivity(), MAIL_DEVELOPER);
	}
}
