package com.tutacode;

import com.tutautils.core.Utils;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class FrgApiRestLogIn extends Fragment{
	LinearLayout llApiUserInputs;
	LinearLayout llApiUserActions;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_apirest_login, container, false);
		llApiUserInputs = (LinearLayout) view.findViewById(R.id.llApiUserInputs);
		llApiUserActions = (LinearLayout) view.findViewById(R.id.llApiUserActions);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		SetLoginScreen();
	}
	
	private void SetLoginScreen(){
		ClearScreen();
		AddLogInInputs();
		AddLogInActions();
	}
	private void SetSignUpScreen(){
		ClearScreen();
		AddSignUpInputs();
		AddSignUpActions();
	}
	private void SetForgotPasswordScreen(){
		ClearScreen();
		AddForgotPwdInputs();
		AddForgotPwdActions();
	}
	private void ClearScreen(){
		llApiUserActions.removeAllViews();
		llApiUserInputs.removeAllViews();
	}
	
	private EditText AddInput(String hint){
		EditText editText = new EditText(getActivity());
		editText.setHint(hint);
		llApiUserInputs.addView(editText);
		return editText;
	}
	
	private void AddAction(String text, OnClickListener onClickListener){
		Button button = new Button(getActivity());
		button.setText(text);
		button.setOnClickListener(onClickListener);
		llApiUserActions.addView(button);
	}
	
	EditText etLoginUserMail, etLoginUserPassword;
	private void AddLogInInputs(){
		etLoginUserMail = AddInput(getString(R.string.apiuser_login_input_usermail));
		etLoginUserPassword = AddInput(getString(R.string.apiuser_login_input_userpassword));
	}
	private void AddLogInActions(){
		AddAction(getString(R.string.apiuser_login_action_login), new OnClickListener() {
			@Override
			public void onClick(View v) {
				doLogin(etLoginUserMail.getText().toString(), etLoginUserPassword.getText().toString());
			}
		});
		AddAction(getString(R.string.apiuser_login_action_signup), new OnClickListener() {
			@Override
			public void onClick(View v) {
				SetSignUpScreen();
			}
		});
		AddAction(getString(R.string.apiuser_login_action_forgot), new OnClickListener() {
			@Override
			public void onClick(View v) {
				SetForgotPasswordScreen();
			}
		});
	}
	
	EditText etSignUpUserNick, etSignUpUserMail, etSignUpUserPassword1, etSignUpUserPassword2; 
	private void AddSignUpInputs(){
		etSignUpUserNick = AddInput(getString(R.string.apiuser_signup_input_usernick));
		etSignUpUserMail = AddInput(getString(R.string.apiuser_signup_input_usermail));
		etSignUpUserPassword1 = AddInput(getString(R.string.apiuser_signup_input_userpassword1));
		etSignUpUserPassword2 = AddInput(getString(R.string.apiuser_signup_input_userpassword2));
	}
	private void AddSignUpActions(){
		AddAction(getString(R.string.apiuser_signup_action_signup), new OnClickListener() {
			@Override
			public void onClick(View v) {
				doSignUp(etSignUpUserNick.getText().toString(), etSignUpUserMail.getText().toString(), 
						etSignUpUserPassword1.getText().toString(), etSignUpUserPassword2.getText().toString());
			}
		});
		AddAction(getString(R.string.apiuser_signup_action_cancel), new OnClickListener() {
			@Override
			public void onClick(View v) {
				SetLoginScreen();
			}
		});
	}
	
	EditText etForgotUserMail;
	private void AddForgotPwdInputs(){
		etForgotUserMail = AddInput(getString(R.string.apiuser_forgot_input_usermail));
	}
	private void AddForgotPwdActions(){
		AddAction(getString(R.string.apiuser_forgot_action_send), new OnClickListener() {
			@Override
			public void onClick(View v) {
				doForgotPassword(etForgotUserMail.getText().toString());
			}
		});
		AddAction(getString(R.string.apiuser_forgot_action_cancel), new OnClickListener() {
			@Override
			public void onClick(View v) {
				SetLoginScreen();
			}
		});
	}

	
	private void doLogin(String mail, String password){
		if (!Utils.isValidEmail(mail)) {
			Toast.makeText(getActivity(), R.string.apiuser_toast_invalidmail, Toast.LENGTH_SHORT).show();
			return;
		}
		if (Utils.isNullOrEmpty(password) || password.length() < 8) {
			Toast.makeText(getActivity(), R.string.apiuser_toast_invalidpassword, Toast.LENGTH_SHORT).show();
			return;
		}
		
	}
	private void doSignUp(String nick, String email, String password1, String password2){
		if(Utils.isNullOrEmpty(nick)){
			Toast.makeText(getActivity(), R.string.apiuser_toast_invalidnick, Toast.LENGTH_SHORT).show();
			return;
		}
		if (!Utils.isValidEmail(email)) {
			Toast.makeText(getActivity(), R.string.apiuser_toast_invalidmail, Toast.LENGTH_SHORT).show();
			return;
		}
		if (Utils.isNullOrEmpty(password1) || password1.length() < 8) {
			Toast.makeText(getActivity(), R.string.apiuser_toast_invalidpassword, Toast.LENGTH_SHORT).show();
			return;
		}
		if (!password1.equals(password2)){
			Toast.makeText(getActivity(), R.string.apiuser_toast_invalidpassword2, Toast.LENGTH_SHORT).show();
			return;
		}
	}
	private void doForgotPassword(String email){
		if (!Utils.isValidEmail(email)) {
			Toast.makeText(getActivity(), R.string.apiuser_toast_invalidmail, Toast.LENGTH_SHORT).show();
			return;
		}
	}
}
