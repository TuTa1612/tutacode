package com.tutacode.social;

import java.util.Arrays;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Request.Callback;
import com.facebook.Request.GraphUserCallback;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.NewPermissionsRequest;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.FacebookDialog.PendingCall;
import com.facebook.widget.LoginButton;
import com.facebook.widget.LoginButton.UserInfoChangedCallback;
import com.facebook.widget.ProfilePictureView;
import com.tutacode.R;
import com.tutautils.core.Utils;

public class FrgFacebook extends Fragment{
	private static final String TAG = "FrgLoginServices";
	private UiLifecycleHelper uiHelper;
	private Session currentSession;
	
	private ProfilePictureView imgFBAvatar;
	private TextView tvFBNombre;
	private Button btnFBShare;
	private Button btnFBUpdateStatus;
	private EditText tvFBPost;
	private Button btnFBPost;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(getActivity(), statusCallback);
		uiHelper.onCreate(savedInstanceState);
	}
		
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_facebook, container, false);
		LoginButton facebookButton = (LoginButton) view.findViewById(R.id.btnFacebookConnect);
		facebookButton.setFragment(this);
		//facebookButton.setReadPermissions(Arrays.asList("user_location", "user_birthday", "user_likes"));
		//facebookButton.setPublishPermissions(Arrays.asList("publish_actions"));
		facebookButton.setUserInfoChangedCallback(new UserInfoChangedCallback() {
			@Override
			public void onUserInfoFetched(GraphUser user) {}
		});
		imgFBAvatar = (ProfilePictureView) view.findViewById(R.id.imgFacebookAvatar);
		tvFBNombre = (TextView) view.findViewById(R.id.tvFacebookName);
		
		btnFBShare = (Button) view.findViewById(R.id.btnFacebookShare);
		btnFBShare.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(getActivity())
						.setLink("https://play.google.com/store/apps/details?id=com.tutacode").build();
				uiHelper.trackPendingDialogCall(shareDialog.present());
			}
		});
		
		btnFBUpdateStatus = (Button) view.findViewById(R.id.btnFacebookUpdateStatus);
		btnFBUpdateStatus.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(getActivity())
						.setLink(null).build();
				uiHelper.trackPendingDialogCall(shareDialog.present());
			}
		});
		
		tvFBPost = (EditText) view.findViewById(R.id.etFacebookPost);
		btnFBPost = (Button) view.findViewById(R.id.btnFacebookPost);
		btnFBPost.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(currentSession == null){
					Toast.makeText(getActivity(), R.string.social_Facebook_toast_mustlogin, Toast.LENGTH_LONG).show();
				}else if (Utils.isNullOrEmpty(tvFBPost.getText().toString())) {
					Toast.makeText(getActivity(), R.string.social_Facebook_toast_statusempty, Toast.LENGTH_LONG).show();
				} else {
					customUpdateStatus();
				}
				
			}
		});
		return view;
	}
	
	private Session.StatusCallback statusCallback = new StatusCallback() {
		
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};
	
	private void  onSessionStateChange(Session session, SessionState state, Exception exception) {
	    if (state.isOpened()) {
	        Log.i(TAG, "Logged in...");
	        currentSession = session;
	        Request.newMeRequest(session, new GraphUserCallback() {
				@Override
				public void onCompleted(GraphUser user, Response response) {
					updateUI(user);
				}
			}).executeAsync();
	    } else if (state.isClosed()) {
	        Log.i(TAG, "Logged out...");
	        updateUI(null);
	    }
	}

	@Override
	public void onResume() {
		super.onResume();
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed()) ) {
			onSessionStateChange(session, session.getState(), null);
		} else {
			//TODO:updateUI(null);
		}
		uiHelper.onResume();
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
			@Override
			public void onError(PendingCall pendingCall, Exception error, Bundle data) {
				Log.e("Activity", String.format("Error: %s", error.toString()));
			}
			
			@Override
			public void onComplete(PendingCall pendingCall, Bundle data) {
				 Log.i("Activity", "Success!");
			}
		});
	}
	
	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}
	
	private void updateUI(GraphUser user){
		if (user!=null) {
			imgFBAvatar.setProfileId(user.getId());
			tvFBNombre.setText(user.getFirstName() + " " + user.getLastName());
		} else {
			imgFBAvatar.setProfileId(null);
			tvFBNombre.setText(null);
		}
		imgFBAvatar.setEnabled(user!=null);
		tvFBNombre.setEnabled(user!=null);
		btnFBShare.setEnabled(user!=null);
		btnFBUpdateStatus.setEnabled(user!=null);
		tvFBPost.setEnabled(user!=null);
		btnFBPost.setEnabled(user!=null);
	}
	
	private void customUpdateStatus(){
		List<String> permisos = currentSession.getPermissions();
		if (!permisos.contains("publish_actions")) {
			NewPermissionsRequest newPermissionRequest = new NewPermissionsRequest(getActivity(), Arrays.asList("publish_actions"));
			currentSession.requestNewPublishPermissions(newPermissionRequest);
			return;
		}
		/*Request.newStatusUpdateRequest(currentSession, "TuTaCode: "+tvFBPost.getText().toString(), new Callback() {
			@Override
			public void onCompleted(Response response) {
				tvFBPost.setText("");
				Toast.makeText(getActivity(), "newStatusUpdateRequest OK", Toast.LENGTH_LONG).show();
			}
		});*/
		Request.executeStatusUpdateRequestAsync(currentSession, "TuTaCode: "+tvFBPost.getText().toString(), new Callback() {
			
			@Override
			public void onCompleted(Response response) {
				tvFBPost.setText("");
				Toast.makeText(getActivity(), R.string.social_Facebook_updatestatus_ok, Toast.LENGTH_LONG).show();
			}
		});
	}
}
