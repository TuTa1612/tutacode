package com.tutacode.social;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

public class SNSHelper {
	public static String getKeyHash(Context ctx){
		try {
            PackageInfo info = ctx.getPackageManager().getPackageInfo("com.tutacode", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String sign=Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("MY KEY HASH:", sign);
                Toast.makeText(ctx ,sign, Toast.LENGTH_LONG).show();
                return sign;
            }
		} catch (NameNotFoundException e) {
			return null;
		} catch (NoSuchAlgorithmException e) {
			return null;
		} 
		return null;
	}
}
